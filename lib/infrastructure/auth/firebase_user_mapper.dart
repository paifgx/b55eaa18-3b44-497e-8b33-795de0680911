import 'package:firebase_auth/firebase_auth.dart' as firebase;
import 'package:flutterapp/domain/auth/user.dart';
import 'package:flutterapp/domain/core/value_objects.dart';

extension FirebaseUserDomainExt on firebase.User {
  User toDomain() => User(id: UniqueId.fromUniqueString(uid));
}
