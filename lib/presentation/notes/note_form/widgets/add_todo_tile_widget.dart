import 'package:kt_dart/kt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutterapp/presentation/notes/note_form/misc/build_context_ext.dart';
import 'package:flutterapp/application/notes/note_form/note_form_bloc.dart';
import 'package:flutterapp/presentation/notes/note_form/misc/todo_item_presentation_classes.dart';

class AddTodoTile extends StatelessWidget {
  const AddTodoTile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      BlocConsumer<NoteFormBloc, NoteFormState>(
        listenWhen: (prev, cur) => prev.isEditing != cur.isEditing,
        listener: (context, state) {
          context.formTodos = state.note.todos.value.fold(
            // in case of failure
            (_) => listOf<TodoItemPrimitive>(),
            // in case of success
            (list) => list.map((_) => TodoItemPrimitive.fromDomain(_)),
          );
        },
        buildWhen: (prev, cur) =>
            prev.note.todos.isFull != cur.note.todos.isFull,
        builder: (context, state) => Padding(
          padding: const EdgeInsets.all(12.0),
          child: ListTile(
            enabled: !state.note.todos.isFull,
            title: const Text('Add a todo'),
            leading: const Icon(Icons.add),
            onTap: () {
              context.formTodos =
                  context.formTodos.plusElement(TodoItemPrimitive.empty());

              context
                  .bloc<NoteFormBloc>()
                  .add(NoteFormEvent.todosChanged(context.formTodos));
            },
          ),
        ),
      );
}
