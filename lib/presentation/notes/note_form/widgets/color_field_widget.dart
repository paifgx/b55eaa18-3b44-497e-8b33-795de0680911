import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterapp/application/notes/note_form/note_form_bloc.dart';
import 'package:flutterapp/domain/notes/value_objects.dart';

class ColorFieldWidget extends StatelessWidget {
  const ColorFieldWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<NoteFormBloc, NoteFormState>(
        buildWhen: (prev, cur) => prev.note.color != cur.note.color,
        builder: (context, state) => Container(
          height: 80,
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: ListView.separated(
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: NoteColor.predefinedColors.length,
            itemBuilder: (context, index) {
              final itemColor = NoteColor.predefinedColors[index];

              return GestureDetector(
                onTap: () => context
                    .bloc<NoteFormBloc>()
                    .add(NoteFormEvent.colorChanged(itemColor)),
                child: Material(
                  color: itemColor,
                  elevation: 4,
                  shape: CircleBorder(
                    side: state.note.color.value.fold(
                      (_) => BorderSide.none,
                      (color) => color == itemColor
                          ? const BorderSide(width: 1.5)
                          : BorderSide.none,
                    ),
                  ),
                  child: Container(width: 50, height: 50),
                ),
              );
            },
            separatorBuilder: (context, index) => const SizedBox(width: 12),
          ),
        ),
      );
}
